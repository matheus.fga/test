package testPessoa;

import static org.junit.Assert.*;

import model.Pessoa;

import org.junit.Before;
import org.junit.Test;



public class PessoaTest {
	
	private Pessoa umaPessoa;
	
	@Before
	public void setUp()throws Exception{
		umaPessoa = new Pessoa();
	}

	@Test
	public void testNome() {
		umaPessoa.setNome("Matheus");
		assertEquals("Matheus", umaPessoa.getNome());
		
	}
	
	@Test
	public void testIdade() {
		umaPessoa.setIdade("18");
		assertEquals("18", umaPessoa.getIdade());
		
	}
	
	@Test
	public void testTelefone() {
		umaPessoa.setTelefone("555-555");
		assertEquals("555-555", umaPessoa.getTelefone());
		
	}
	
	@Test
	public void testSexo() {
		umaPessoa.setSexo("M");
		assertEquals("M", umaPessoa.getSexo());
		
	}
	
	@Test
	public void testEmail() {
		umaPessoa.setEmail("matheussilva.fga@gmail.com");
		assertEquals("matheussilva.fga@gmail.com", umaPessoa.getEmail());
		
	}
	
	@Test
	public void testHangout() {
		umaPessoa.setHangout("333-333");
		assertEquals("333-333", umaPessoa.getHangout());
		
	}
	
	@Test
	public void testEndereco() {
		umaPessoa.setEndereco("QS05");
		assertEquals("QS05", umaPessoa.getEndereco());
		
	}
	
	@Test
	public void testRg() {
		umaPessoa.setRg("111-111");
		assertEquals("111-111", umaPessoa.getRg());
		
	}
	
	@Test
	public void testCpf() {
		umaPessoa.setCpf("xx-xxxx");
		assertEquals("xx-xxxx", umaPessoa.getCpf());
		
	}

}
