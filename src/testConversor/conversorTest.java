package testConversor;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import conversor.ConversorTemperatura;

public class conversorTest {
	
	private ConversorTemperatura meuConversor;
	
	@Before
	public void setUp()throws Exception{
		meuConversor = new ConversorTemperatura();
	}

	@Test
	public void testCelsiusParaFahrenheitComValorCelsiusDouble() {
		assertEquals(104.0, meuConversor.CelsiusParaFahrenheit(40.0), 0.01);
		
	}
	
	@Test
	public void testFahrenheitParaCelsiusComValorFahrenheitDouble() {
		assertEquals(40.0, meuConversor.FahrenheitParaCelsius(104.0), 0.01);
		
	}
	
	@Test
	public void testCelsiusParaKelvinComValorCelsiusDouble() {
		assertEquals(273 , meuConversor.CelsiusParaKelvin(0), 0.01);
		
	}
	
	@Test
	public void testKelvinParaCelsiusComValorKelvinDouble() {
		assertEquals(0 , meuConversor.KelvinParaCelsius(273), 0.01);
		
	}
	
	@Test
	public void testFahrenheitParaKelvinComValorFahrenheitDouble() {
		assertEquals(373 , meuConversor.FahrenheitParaKelvin(212), 0.01);
		
	}
	
	@Test
	public void testKelvinParaFahrenheitComValorKelvinDouble() {
		assertEquals(212 , meuConversor.KelvinParaFahrenheit(373), 0.01);
		
	}

}
