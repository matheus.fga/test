package testControle;

import static org.junit.Assert.*;

import model.Pessoa;

import org.junit.Before;
import org.junit.Test;

import controle.ControlePessoa;

public class controleTest {
	
private ControlePessoa umaPessoaControle;
private Pessoa umaPessoa;
	
	@Before
	public void setUp()throws Exception{
		umaPessoaControle = new ControlePessoa();
		umaPessoa = new Pessoa();
	}

	@Test
	public void testAdicionar() {		
		assertEquals("Pessoa adicionada com Sucesso!", umaPessoaControle.adicionar(umaPessoa));
		
	}
	
	@Test
	public void testRemover() {		
		assertEquals("Pessoa removida com Sucesso!", umaPessoaControle.remover(umaPessoa));
		
	}
	
	@Test
	public void testPesquisar() {
		String umNome = "Matheus";
		umaPessoa.setNome(umNome);
		umaPessoaControle.adicionar(umaPessoa);
		
		assertEquals(umaPessoa, umaPessoaControle.pesquisar(umNome));
		
	}

}
