package conversor;

public class ConversorTemperatura {
	
	public double CelsiusParaFahrenheit (double valorCelsius){
		return 9*valorCelsius/5+32;
	}
	
	public double FahrenheitParaCelsius (double valorFahrenheit){
		return 5*(valorFahrenheit - 32)/9;
	}
	
	public double CelsiusParaKelvin (double valorCelsius){
		return valorCelsius + 273;
	}
	
	public double KelvinParaCelsius (double valorKelvin){
		return valorKelvin - 273;
	}
	
	public double FahrenheitParaKelvin (double valorFahrenheit){
		return 5*(valorFahrenheit - 32)/9 + 273;
	}
	
	public double KelvinParaFahrenheit (double valorKelvin){
		return 1.8*(valorKelvin - 273) + 32;
	}
}
